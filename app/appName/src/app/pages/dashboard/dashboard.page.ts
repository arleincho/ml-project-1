    import { Component, OnInit } from '@angular/core';
    import { MenuController } from '@ionic/angular';
    import { AuthService } from 'src/app/services/auth.service';
    import { User } from 'src/app/models/user';
    import { NgForm } from '@angular/forms';
    import { AlertService } from 'src/app/services/alert.service';
    import { FormGroup, FormBuilder,FormControl, Validators } from '@angular/forms';

    @Component({
      selector: 'app-dashboard',
      templateUrl: './dashboard.page.html',
      styleUrls: ['./dashboard.page.scss'],
    })
    export class DashboardPage implements OnInit {
      user: User;
      formPredict : FormGroup;

      constructor(
        private formBuilder : FormBuilder,
        private alertService: AlertService,
        private menu: MenuController,
        private authService: AuthService) {

          this.menu.enable(true);


          this.formPredict = this.formBuilder.group({

            Contract : new FormControl('', Validators.compose([Validators.required])),
            tenure : new FormControl(0, Validators.compose([Validators.required])),
            InternetService : new FormControl('', Validators.compose([Validators.required])),
            PaymentMethod : new FormControl('', Validators.compose([Validators.required])),
            PaperlessBilling : new FormControl(true, Validators.compose([Validators.required])),
            OnlineSecurity : new FormControl('', Validators.compose([Validators.required])),
            TechSupport : new FormControl('', Validators.compose([Validators.required])),
            Dependents : new FormControl(true, Validators.compose([Validators.required])),
            Partner : new FormControl(true, Validators.compose([Validators.required])),
            SeniorCitizen : new FormControl(true, Validators.compose([Validators.required])),
            OnlineBackup : new FormControl('', Validators.compose([Validators.required])),
            DeviceProtection : new FormControl('', Validators.compose([Validators.required])),
            StreamingTV : new FormControl('', Validators.compose([Validators.required])),
            MonthlyCharges : new FormControl(0, Validators.compose([Validators.required])),

          });



      }
      ngOnInit() {
        
      }
      ionViewWillEnter() {
        this.authService.user().subscribe(
          user => {
            this.user = user;
          }
        );
      }

      prediction() {

        document.getElementById('result').innerHTML = ``;

        let me = this;    

        if(me.formPredict.valid){

          this.authService.prediction(me.formPredict.value).subscribe(
            data => {
              this.alertService.presentToast(data);

              // console.log(data['predict'])
              // console.log(data['value'][0])

              let opciones = ["No", ""];

                document.getElementById('result').innerHTML = `
                    <div>
                      The customer will be <b><i> ` + opciones[data['value'][0]] + ` Churn</i><b> in a %` + new Intl.NumberFormat('en-US', {
                        maximumSignificantDigits: 3 })
                        .format(data['predict'][0][data['value'][0]]) + `
                    </div>
                  `;

              // console.log("se agregan estos campos", data);
            },
            error => {
              console.log(error);
              this.alertService.presentToast(error.statusText);
            }
          );
        } else {
            this.alertService.presentToast("empty fields");
        }  
      }
    }