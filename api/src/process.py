#!/usr/bin/env python
# coding: utf-8
# Data Analysis & Preprocessing
import math
import pandas as pd
import numpy as np
import pickle

from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from scipy.stats import norm
from sklearn.preprocessing import LabelBinarizer
from sklearn import preprocessing

# Visualization
import matplotlib.pyplot as plt
import seaborn as sns
#get_ipython().run_line_magic('matplotlib', 'inline')

def prepare(df_train):
    #df_train = pd.read_csv('watson.csv')
    # Convert to String Data Type
    df_train['SeniorCitizen'] = df_train['SeniorCitizen'].astype(str)
    df2 = pd.isnull(df_train)
    result = False
    for col in df_train.columns: 
        result = result | (df2[col] == True)
    df_train = df_train.loc[~result]
    df_train_cleaned = df_train.copy()
    criteria = [df_train_cleaned['tenure'].between(0, 19), df_train_cleaned['tenure'].between(20, 39), 
                df_train_cleaned['tenure'].between(40, 59), df_train_cleaned['tenure'].between(60, np.inf)]
    values = ['Range1','Range2','Range3','Range4']
    df_train_cleaned['tenureCount'] = np.select(criteria, values, 0)
    df_train['OnlineSecurity'] = df_train['OnlineSecurity'].map({'No internet service': 'No','No': 'No','Yes': 'Yes'})
    df_train['OnlineBackup'] = df_train['OnlineBackup'].map({'No internet service': 'No','No': 'No','Yes': 'Yes'})
    df_train['DeviceProtection'] = df_train['DeviceProtection'].map({'No internet service': 'No','No': 'No','Yes': 'Yes'})
    df_train['TechSupport'] = df_train['TechSupport'].map({'No internet service': 'No','No': 'No','Yes': 'Yes'})
    df_train['StreamingTV'] = df_train['StreamingTV'].map({'No internet service': 'No','No': 'No','Yes': 'Yes'})
    #df_train['StreamingMovies'] = df_train['StreamingMovies'].map({'No internet service': 'No','No': 'No','Yes': 'Yes'})
    #df_train['MultipleLines'] = df_train['MultipleLines'].map({'No phone service': 'No','No': 'No','Yes': 'Yes'})
    df_train['tenure'] = df_train_cleaned['tenureCount']

    #pd.Dataframe(columns = )
    
    tenure_Range1 = 0
    tenure_Range2 = 0
    tenure_Range3 = 0
    tenure_Range4 = 0
    Partner_Yes = 0
    Dependents_Yes = 0
    InternetService_DSL = 0
    InternetService_Fiber_optic = 0
    InternetService_No = 0
    OnlineSecurity_Yes = 0
    OnlineBackup_Yes = 0
    DeviceProtection_Yes = 0
    TechSupport_Yes = 0
    StreamingTV_Yes = 0
    Contract_Month_to_month = 0
    Contract_One_year = 0
    Contract_Two_year = 0
    PaperlessBilling_Yes = 0
    PaymentMethod_Bank_transfer = 0
    PaymentMethod_Credit_card = 0
    PaymentMethod_Electronic_check = 0
    PaymentMethod_Mailed_check = 0
    SeniorCitizen = 0
    MonthlyCharges = df_train['MonthlyCharges'].iloc[0]
    MonthlyCharges = (MonthlyCharges - 18.25)/(118.75-18.25)
    

    if (df_train['tenure'].iloc[0] == 'Range1') : tenure_Range1 = 1
    if (df_train['tenure'].iloc[0] == 'Range2') : tenure_Range2 = 1
    if (df_train['tenure'].iloc[0] == 'Range3') : tenure_Range3 = 1
    if (df_train['tenure'].iloc[0] == 'Range4') : tenure_Range4 = 1
    if (df_train['Partner'].iloc[0] == True) : Partner_Yes = 1
    if (df_train['Dependents'].iloc[0] == True) : Dependents_Yes = 1
    if (df_train['InternetService'].iloc[0] == 'DSL') : InternetService_DSL = 1
    if (df_train['InternetService'].iloc[0] == 'Fiber optic') : InternetService_Fiber_optic = 1
    if (df_train['InternetService'].iloc[0] == 'No') : InternetService_No = 1
    if (df_train['OnlineSecurity'].iloc[0] == 'Yes') : OnlineSecurity_Yes = 1
    if (df_train['OnlineBackup'].iloc[0] == 'Yes') : OnlineBackup_Yes = 1
    if (df_train['DeviceProtection'].iloc[0] == 'Yes') : DeviceProtection_Yes = 1
    if (df_train['TechSupport'].iloc[0] == 'Yes') : TechSupport_Yes = 1
    if (df_train['StreamingTV'].iloc[0] == 'Yes') : StreamingTV_Yes = 1
    if (df_train['SeniorCitizen'].iloc[0] == True) : SeniorCitizen = 1 
    if (df_train['Contract'].iloc[0] == 'One year') : Contract_One_year = 1
    if (df_train['Contract'].iloc[0] == 'Two year') : Contract_Two_year = 1
    if (df_train['Contract'].iloc[0] == 'Month-to-month') : Contract_Month_to_month = 1
    if (df_train['PaperlessBilling'].iloc[0] == True) : PaperlessBilling_Yes = 1
    if (df_train['PaymentMethod'].iloc[0] == 'Bank transfer (automatic)') : PaymentMethod_Bank_transfer = 1
    if (df_train['PaymentMethod'].iloc[0] == 'Credit card (automatic)') : PaymentMethod_Credit_card = 1
    if (df_train['PaymentMethod'].iloc[0] == 'Electronic check') : PaymentMethod_Electronic_check = 1
    if (df_train['PaymentMethod'].iloc[0] == 'Mailed check') : PaymentMethod_Mailed_check = 1


    d = {
        'SeniorCitizen':SeniorCitizen ,
        'tenure_Range1': tenure_Range1,
        'tenure_Range2': tenure_Range2,
        'tenure_Range3': tenure_Range3,
        'tenure_Range4': tenure_Range4,
        'MonthlyCharges': MonthlyCharges,
        'Partner_Yes': Partner_Yes,
        'Dependents_Yes': Dependents_Yes,
        'InternetService_DSL': InternetService_DSL,
        'InternetService_Fiber optic': InternetService_Fiber_optic ,
        'InternetService_No': InternetService_No,
        'OnlineSecurity_Yes': OnlineSecurity_Yes,
        'OnlineBackup_Yes': OnlineBackup_Yes,
        'DeviceProtection_Yes': DeviceProtection_Yes,
        'TechSupport_Yes': TechSupport_Yes,
        'StreamingTV_Yes': StreamingTV_Yes,
        'Contract_Month-to-month': Contract_Month_to_month,
        'Contract_One year': Contract_One_year,
        'Contract_Two year': Contract_Two_year,
        'PaperlessBilling_Yes': PaperlessBilling_Yes,
        'PaymentMethod_Bank transfer (automatic)': PaymentMethod_Bank_transfer,
        'PaymentMethod_Credit card (automatic)': PaymentMethod_Credit_card ,
        'PaymentMethod_Electronic check': PaymentMethod_Electronic_check,
        'PaymentMethod_Mailed check':PaymentMethod_Mailed_check,
    }

    X_all = pd.DataFrame.from_dict([d])
    return X_all


def predicting(df_train):
    pickle_in = open('finalmodel.pickle2','rb')
    final_model = pickle.load(pickle_in)

    #predicting with new data
    # accuracy  = final_model.score(X_test,y_test)
    chun_prediction = final_model.predict_proba(df_train)
    chun_prediction_2 = final_model.predict(df_train)
    #print(churn_predict,accuracy)
    print("este es el resultado %s : %s"  % (chun_prediction, chun_prediction_2))

    return {'predict': chun_prediction.tolist(), 'value': chun_prediction_2.tolist()}




