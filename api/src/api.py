#!/usr/bin/env python3
#
# Copyright 2009 Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import aiopg
import bcrypt
import markdown
import os.path
import psycopg2
import re
import tornado.escape
import tornado.httpserver
import tornado.ioloop
import tornado.locks
import tornado.web
import unicodedata
import secrets
import pandas as pd

from process import prepare, predicting


db_host = os.environ['POSTGRES_HOST']
db_user = os.environ['POSTGRES_USER']
db_password = os.environ['POSTGRES_PASSWORD']
db_port = os.environ['POSTGRES_PORT']
db_database = os.environ['POSTGRES_DB']

api_port = os.environ['API_PORT']

token_type = "MachineLearning"


class NoResultError(Exception):
    pass


class Application(tornado.web.Application):
    def __init__(self, db):

        self.db = db
        handlers = [
            (r"/auth/login/", AuthLoginHandler),
            (r"/auth/logout", AuthLogoutHandler),
            (r"/auth/predict", AuthPredictHandler),
            (r"/auth/user", UserHandler)
        ]
        settings = dict(
            cookie_secret="Movb9mdCSSCjI3SoOjNOrcRoXhfZdz04RqFJiUdDWsp3EOPvjVTOYkHyR5exwP5Z22rpM",
            debug=True,
        )
        super(Application, self).__init__(handlers, **settings)


class BaseHandler(tornado.web.RequestHandler):

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Access-Control-Allow-Headers", "cache-control,Content-Type,X-Amz-Date,Authorization,X-Api-Key,Origin,Accept,Access-Control-Allow-Headers,Access-Control-Allow-Methods,Access-Control-Allow-Origin")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')




    def row_to_obj(self, row, cur):
        """Convert a SQL row to an object supporting dict and attribute access."""
        obj = tornado.util.ObjectDict()
        for val, desc in zip(row, cur.description):
            obj[desc.name] = val
        return obj

    async def options(self):
        # no body
        self.set_status(200)
        return


    async def execute(self, stmt, *args):
        """Execute a SQL statement.

        Must be called with ``await self.execute(...)``
        """
        with (await self.application.db.cursor()) as cur:
            await cur.execute(stmt, args)

    async def query(self, stmt, *args):
        """Query for a list of results.

        Typical usage::

            results = await self.query(...)

        Or::

            for row in await self.query(...)
        """
        with (await self.application.db.cursor()) as cur:
            await cur.execute(stmt, args)
            return [self.row_to_obj(row, cur) for row in await cur.fetchall()]

    async def queryone(self, stmt, *args):
        """Query for exactly one result.

        Raises NoResultError if there are no results, or ValueError if
        there are more than one.
        """
        results = await self.query(stmt, *args)
        if len(results) == 0:
            raise NoResultError()
        elif len(results) > 1:
            raise ValueError("Expected 1 result, got %d" % len(results))
        return results[0]

    async def prepare(self):
        # get_current_user cannot be a coroutine, so set
        # self.current_user in prepare instead.
        user_id = self.get_secure_cookie("blogdemo_user")
        if user_id:
            self.current_user = await self.queryone(
                "SELECT * FROM authors WHERE id = %s", int(user_id)
            )



class AuthLoginHandler(BaseHandler):

    async def post(self):
        try:
            params = tornado.escape.json_decode(self.request.body)
            user = await self.queryone(
                "SELECT * FROM users WHERE email = %s", params["email"]
            )
        except NoResultError:
            raise tornado.web.HTTPError(401)
            return
        hashed_password = await tornado.ioloop.IOLoop.current().run_in_executor(
            None,
            bcrypt.hashpw,
            tornado.escape.utf8(params["password"]),
            tornado.escape.utf8(user.hashed_password),
        )
        hashed_password = tornado.escape.to_unicode(hashed_password)
        if hashed_password == user.hashed_password:
            token = secrets.token_urlsafe()
            try:
                await self.execute('UPDATE users SET token = %s WHERE id = %s',
                    token,
                    user.id
                )
                self.write({"access_token": token, "token_type": token_type})
            except Exception:
                raise tornado.web.HTTPError()
        else:
            raise tornado.web.HTTPError(401)

  

class AuthLogoutHandler(BaseHandler):

    async def get(self):
        try:
            token = self.request.headers.get('Authorization')

            auth_header = self.request.headers.get("Authorization", "")

            if not auth_header.startswith("%s " % (token_type,)):
                raise tornado.web.HTTPError(401)
                return
            else:
                auth_decoded = auth_header[len(token_type):].strip()

                user = await self.execute(
                    "UPDATE  users SET token = '' WHERE token = %s", auth_decoded
                )
                self.write({"data": "Logout correctly"})

        except NoResultError:
            raise tornado.web.HTTPError(401)
            return

  
class AuthPredictHandler(BaseHandler):

    async def post(self):
        try:
            token = self.request.headers.get('Authorization')

            auth_header = self.request.headers.get("Authorization", "")

            if not auth_header.startswith("%s " % (token_type,)):
                raise tornado.web.HTTPError(401)
                return
            else:
                auth_decoded = auth_header[len(token_type):].strip()

                user = await self.queryone(
                    "SELECT id, email, name FROM users WHERE token = %s", auth_decoded
                )

                params = tornado.escape.json_decode(self.request.body)
                data = pd.DataFrame.from_dict([params])

                print("estamos recibiendo %s:: %s" % (params, data))

                data_process = prepare(data)
                chun_prediction = predicting(data_process)

                self.write(chun_prediction)

        except NoResultError:
            raise tornado.web.HTTPError(401)
            return

        # except Exception:
        #     raise tornado.web.HTTPError()


class UserHandler(BaseHandler):

    async def get(self):
        try:
            token = self.request.headers.get('Authorization')

            auth_header = self.request.headers.get("Authorization", "")

            if not auth_header.startswith("%s " % (token_type,)):
                raise tornado.web.HTTPError(401)
                return
            else:
                auth_decoded = auth_header[len(token_type):].strip()

                user = await self.queryone(
                    "SELECT id, email, name FROM users WHERE token = %s", auth_decoded
                )

                self.write({"user": user})

        except NoResultError:
            raise tornado.web.HTTPError(401)
            return

        except Exception:
            raise tornado.web.HTTPError()
  

async def main():

    # Create the global connection pool.
    async with aiopg.create_pool(
        host=db_host,
        port=db_port,
        user=db_user,
        password=db_password,
        dbname=db_database,
    ) as db:
        app = Application(db)
        app.listen(api_port)

        # In this demo the server will simply run until interrupted
        # with Ctrl-C, but if you want to shut down more gracefully,
        # call shutdown_event.set().
        shutdown_event = tornado.locks.Event()
        await shutdown_event.wait()


if __name__ == "__main__":
    tornado.ioloop.IOLoop.current().run_sync(main)
