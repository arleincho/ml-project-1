#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

	create extension pgcrypto;

	DROP TABLE IF EXISTS users;
	CREATE TABLE users (
	    id SERIAL PRIMARY KEY,
	    email VARCHAR(100) NOT NULL UNIQUE,
	    name VARCHAR(100) NOT NULL,
	    hashed_password VARCHAR(100) NOT NULL,
	    token VARCHAR(100) NOT NULL DEFAULT ''
	);

	INSERT INTO users (email, name, hashed_password) values('$USER_API', '$NAME_API', crypt('$PASS_API', gen_salt('bf')));
EOSQL